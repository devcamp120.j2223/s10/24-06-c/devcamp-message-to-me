import { Component } from "react";

import InputMessage from "./input/InputMessage";
import OutputMessage from "./output/OutputMessage";

class ContentComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            messageInput: "",
            messageOutput: [],
            likeDisplay: false
        }
    }

    messageInputChangeHandler = (value) => {
        this.setState({
            messageInput: value
        })
    }

    messageOutputChangeHandler = () => {
        this.setState({
            messageOutput: [...this.state.messageOutput, this.state.messageInput],
            likeDisplay: this.state.messageInput ? true : false
        })
    }

    render() {
        return (
            <div>
                <InputMessage messageInputProp={this.state.messageInput} messageInputChangeHandlerProp={this.messageInputChangeHandler} messageOutputChangeHandlerProp={this.messageOutputChangeHandler}/>
                <OutputMessage messageOutputProp={this.state.messageOutput} likeDisplayProp={this.state.likeDisplay} />
            </div>
        )
    }
}

export default ContentComponent;