import { Component } from "react";

import backgroundImg from "../../../assets/images/background.jpg";

class TittleImage extends Component {
    render() {
        return (
            <div className="row mt-2">
                <div className="col-12">
                    <img src={backgroundImg} alt="background" width={500}/>
                </div>
            </div>
        )
    }
}

export default TittleImage;