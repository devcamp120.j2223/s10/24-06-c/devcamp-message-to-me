import { Component } from "react";

import TitleText from "./text/TitleText";
import TittleImage from "./image/TitleImage";

class TitleComponent extends Component {
    render() {
        return (
            <div>
                <TitleText />
                <TittleImage />
            </div>
        )
    }
}

export default TitleComponent;